<?php

require_once 'src/controlers/DefaultControler.php';
require_once 'src/controlers/SecurityControler.php';
require_once 'src/controlers/RegisterControler.php';
require_once 'src/controlers/StockControler.php';
require_once 'src/controlers/LogoutController.php';

class Routing{
    public static $routes;

    public static function get($url,$controler){
        self::$routes[$url] = $controler;
    }

    public static function post($url,$controler){
        self::$routes[$url] = $controler;
    }

    public static function run($url){
        $action = explode("/",$url)[0];

        if(!array_key_exists($action,self::$routes)){
          die("Wrong url !");  
        }

        $controler = self::$routes[$action];
        $object = new $controler;
        $action = $action ?: 'projects';

        $object->$action();
    }
}