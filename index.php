<?php

require 'Routing.php';

$path = trim($_SERVER['REQUEST_URI'],'/');
$path = parse_url($path,PHP_URL_PATH);

Routing::get('','DefaultControler');
Routing::get('projects','DefaultControler');
Routing::get('index','DefaultControler');
Routing::post('login','SecurityControler');
Routing::post('register','RegisterControler');
Routing::post('search','DefaultControler');
Routing::get('stock','StockControler');
Routing::get('logout','LogoutController');

Routing::run($path);
