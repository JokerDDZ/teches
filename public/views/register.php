<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/login.css">
    <script type="text/javascript" src="./public/js/script.js" defer></script>
    <title>Register PAGE</title>
</head>
<body>
<div class="container">
    <div class = "login-container">
        <a href="projects">
            <img class="logoImage" src="public/img/logo.svg">
        </a>
        <p class="createAccountLabel">
            Create Acccount
        </p>
        <form class="login" action="register" method="POST">
            <div class="messages">
                <?php if(isset($messages)){
                    foreach($messages as $mess){
                        echo $mess;
                    }
                }
                ?>
            </div>
            <input name ="username" type="text" placeholder="username">
            <input name="email" type="text" placeholder="email@email.com">
            <input name="password" type="password" placeholder="password">
            <input name="confirmedPassword" type="password" placeholder="confirmedPassword">
            <button class  = "loginButton" type="submit">Create Account</button>
        </form>
    </div>
</div>
</body>