<!DOCTYPE html> 
<head>
    <link rel="stylesheet" type="text/css" href="public/css/login.css">
    <script type="text/javascript" src="./public/js/login.js" defer></script>
    <title>LOGIN PAGE</title>
</head>
<body>
    <div class="container">      
        <div class = "login-container">
            <a href="projects">
                <img class="logoImage" src="public/img/logo.svg">
            </a>
            <form class="login" action="login" method="POST">
                <div class="messages">
                    <?php if(isset($messages)){
                        foreach($messages as $mess){
                            echo $mess;  
                        }                                   
                    }
                    ?>
                </div>
                <input name ="username" type="text" placeholder="username">
                <input name="password" type="password" placeholder="password">
                <button class  = "loginButton" type="submit">Login</button>
                <a href="register">Sign up</a>
            </form>
        </div>

    </div>
</body>