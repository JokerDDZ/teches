<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="public/css/stock.css">
    <script src="https://kit.fontawesome.com/b2108bc64f.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.3.2/chart.min.js"></script>
    <script type="text/javascript" src="./public/js/stock.js" defer></script>
    <script type="text/javascript" src="./public/js/login.js" defer></script>
    <title>STOCK</title>
</head>

<body>
<div class="base-container">
    <nav>
        <a href="projects">
            <img href src="public/img/logo.svg">
        </a>
        <ul>
            <li>
                <button href="#" class="button"><i class="fas fa-list"></i> watchlist</button>
            </li>
            <li>
                <button href="#" class="button"><i class="fas fa-cog"></i> settings</button>
            </li>
            <li>
                <?php if( $_SESSION['user']): ?>
                    <button onclick="logoutFunction()"  href="#" class="button" id="mainMenuLoginButton"><i class="fas fa-door-open"></i> logut</button>
                <?php else: ?>
                    <button onclick="loginFunction()"  href="#" class="button" id="mainMenuLogoutButton"><i class="fas fa-door-open"></i> login</button>
                <?php endif; ?>
            </li>
        </ul>
    </nav>
    <main>
        <div class="stockContainer">
            <div class="stockData">
                <div class="stockLogoContainer">
                    <img src="public/img/uploads/Tesla.png">
                    <p>TSLA</p>
                </div>
                <div  class="stockOverview">
                    <p>Overview</p>
                    <div class="stockInfo">
                        <div id="heading">
                            <button class="button0">Beta<p>Number</p></button>
                            <p class="info0">Beta is a measure of the volatility—or systematic risk—of a security or portfolio compared to the market as a whole. Beta is used in the capital asset pricing model (CAPM), which describes the relationship between systematic risk and expected return for assets (usually stocks). CAPM is widely used as a method for pricing risky securities and for generating estimates of the expected returns of assets, considering both the risk of those assets and the cost of capital.</p>
                        </div>
                        <div id="heading">
                            <button class="button1">PE Ratio<p>Number</p></button>
                            <p class="info1">The price-to-earnings ratio (P/E ratio) is the ratio for valuing a company that measures its current share price relative to its per-share earnings (EPS). The price-to-earnings ratio is also sometimes known as the price multiple or the earnings multiple.</p>
                        </div>
                        <div id="heading">
                            <button class="button2">Payout Ratio<p>Number</p></button>
                            <p class="info2">The payout ratio is a financial metric showing the proportion of earnings a company pays its shareholders in the form of dividends, expressed as a percentage of the company's total earnings. On some occasions, the payout ratio refers to the dividends paid out as a percentage of a company's cash flow. The payout ratio is also known as the dividend payout ratio.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="diagramContainer">
                <canvas id="diagramChart" width="500" height="500"></canvas>
            </div>
        </div>
    </main>
</div>
</body>
