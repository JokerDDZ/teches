<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/projects.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/b2108bc64f.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/mainStockSheet.js" defer></script>
    <script type="text/javascript" src="./public/js/login.js" defer></script>
    <title>PROJECTS</title>
</head>

<body>
    <div class="base-container">
        <nav>
            <a href="projects">
                <img href src="public/img/logo.svg">
            </a>
            <ul>
                <li>
                    <button href="#" class="button"><i class="fas fa-list"></i> watchlist</button>
                </li>
                <li>
                    <button href="#" class="button"><i class="fas fa-cog"></i> settings</button>
                </li>
                <li>
                    <?php if( $_SESSION['user']): ?>
                        <button onclick="logoutFunction()"  href="#" class="button" id="mainMenuLoginButton"><i class="fas fa-door-open"></i> logut</button>
                    <?php else: ?>
                        <button onclick="loginFunction()"  href="#" class="button" id="mainMenuLogoutButton"><i class="fas fa-door-open"></i> login</button>
                    <?php endif; ?>
                </li>
            </ul>
        </nav>
        <main>
            <header>
                <div class="search-bar">
                    <input placeholder="search stock">
                </div>
            </header>
            <div class="upper-bar">
                <p>
                    Market
                </p>
                <p>
                    Value
                </p>
                <p>
                    Day's Range
                </p>
                <p>
                    Week Range
                </p>
                <p>
                    Month Range
                </p>
            </div>
            <section class="projects">
            </section>
        </main>
    </div>
</body>
<input type="hidden" id="hdnSession" data-value="@Request.RequestContext.HttpContext.Session['someKey']" />


<template id="project-template">
    <a href="stock" action="stock" method="POST">
        <div>
            <img src="public/img/uploads/Tesla.png">
            <div>
                <p id="name">name</p>
                <p id="value">value</p>
                <p id="day">day</p>
                <p id="week">week</p>
                <p id="month">month</p>
            </div>
        </div>
    </a>
</template>