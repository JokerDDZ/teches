const stockName = document.querySelector(".stockLogoContainer>p")
const betaValue = document.querySelector(".button0>p");
const peRatioValue = document.querySelector(".button1>p");
const payoutRatioValue = document.querySelector(".button2>p");
const diagramCanvas = document.getElementById("diagramChart");

stockName.innerHTML = sessionStorage.getItem('name');
betaValue.innerHTML = sessionStorage.getItem('beta');
peRatioValue.innerHTML = sessionStorage.getItem('peRatio');
payoutRatioValue.innerHTML = sessionStorage.getItem('payoutRatio');

const dailyArray = JSON.parse(sessionStorage.getItem('dailyValue'));

const closeValueArray = Get30DaysClose();
const labelArray = Get30DaysLabel();

var ctx = diagramCanvas.getContext('2d');

const up = (ctx, value) => ctx.p0.parsed.y < ctx.p1.parsed.y ? value : undefined;
const down = (ctx, value) => ctx.p0.parsed.y > ctx.p1.parsed.y ? value : undefined;

let myChart = new Chart(ctx,{
    type:'line',
    data:{
        labels:labelArray.reverse(),
        datasets:[{
            label:'30 Days',
            data:closeValueArray.reverse(),
            segment: {
                borderColor: ctx => down(ctx, 'rgb(192,75,75)') || up(ctx, 'rgb(0,255,0)')
            }
        }]
    },

    options:{}
});

function Get30DaysClose(){
    const key = Object.keys(dailyArray["Time Series (Daily)"]);
    let result = [];

    for(let step = 0;step < 30;step++){
        let toNumber = parseFloat(dailyArray["Time Series (Daily)"][key[step]]["4. close"]);
        result.push(toNumber);
    }

    return result;
}

function Get30DaysLabel(){
    const key = Object.keys(dailyArray["Time Series (Daily)"]);
    let result = [];

    for(let step = 0;step < 30;step++){
        result.push(key[step]);
    }

    return result;
}