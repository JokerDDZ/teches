const APIKEY = "TOHUT4TUNMMD4KBD";
const APIKEYDAILY = "UDW5AKBMDU95FQ7J";
const search = document.querySelector('input[placeholder = "search stock"]');
const projectsContainer = document.querySelector(".projects");
const loginButton = document.getElementById('mainMenuLoginButton');
const logoutButton = document.getElementById('mainMenuLogoutButton');

//goToLoginPageEvent();

search.addEventListener("keyup",function (event){
    if(event.key === "Enter"){
        event.preventDefault();
        fetchStock(search.value);
    }
});

function fetchStock(symbolOfStock){
    apiCallDaily = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol='+ symbolOfStock +'&interval=5min&apikey=' + APIKEYDAILY;
    apiCallCurrent = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=' + symbolOfStock +'&interval=5min&apikey=' + APIKEY;
    apiCallOverview = 'https://www.alphavantage.co/query?function=OVERVIEW&symbol=' + symbolOfStock + '&apikey=' + APIKEY;

    Promise.all([
        fetch(apiCallCurrent).then(value => value.json()),
        fetch(apiCallDaily).then(value => value.json()),
        fetch(apiCallOverview).then(value => value.json())
    ])
        .then((value) => {
            console.log(value)
            projectsContainer.innerHTML = "";
            createProject(value[0],value[1],value[2]);
        })
        .catch((err) => {
            console.log(err);
        });
}

function createProject(current,daily,overview){
    const template = document.querySelector("#project-template");
    const clone = template.content.cloneNode(true);
    const name = clone.querySelector("#name");
    const value = clone.querySelector("#value");
    const day = clone.querySelector("#day");
    const week = clone.querySelector("#week");
    const month = clone.querySelector("#month");

    const stockName = current["Meta Data"]["2. Symbol"];
    name.innerHTML = stockName;

    let currentKey = Object.keys(current["Time Series (5min)"]);
    let close = current["Time Series (5min)"][currentKey[0]]["4. close"];
    value.innerHTML = parseFloat(close).toString();

    let dailyKey = Object.keys(daily["Time Series (Daily)"]);
    let dayLow = daily["Time Series (Daily)"][dailyKey[0]]["3. low"];
    let dayHigh = daily["Time Series (Daily)"][dailyKey[0]]["2. high"];
    day.innerHTML = parseFloat(dayLow) + '-' + parseFloat(dayHigh);

    let weeklyValues = getLowHighBaseOnDays(daily,7);
    week.innerHTML = parseFloat(weeklyValues[0]) + '-' + parseFloat(weeklyValues[1]);

    let monthlyValues = getLowHighBaseOnDays(daily,30);
    month.innerHTML = parseFloat(monthlyValues[0]) + '-' + parseFloat(monthlyValues[1]);

    projectsContainer.appendChild(clone)
    sessionStorage.clear();
    console.log(overview);

    const beta = overview['Beta'];
    const peRatio = overview['PERatio'];
    const payoutRatio = overview['PayoutRatio'];

    sessionStorage.setItem('name',stockName);
    sessionStorage.setItem('beta',beta);
    sessionStorage.setItem('peRatio',peRatio);
    sessionStorage.setItem('payoutRatio',payoutRatio);
    sessionStorage.setItem('dailyValue',JSON.stringify(daily));

};

function getLowHighBaseOnDays(dailyArray,days){
    let dailyKey = Object.keys(dailyArray["Time Series (Daily)"]);

    let low = 9999999;
    let high = -1000;

    for(let step = 0;step < days;step++){
        let lowHandler = dailyArray["Time Series (Daily)"][dailyKey[step]]["3. low"];

        if(low > lowHandler) low = lowHandler;

        let highHandler = dailyArray["Time Series (Daily)"][dailyKey[step]]["2. high"];

        if(highHandler > high) high = highHandler;
    }

    return[low,high];
}