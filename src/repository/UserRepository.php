<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/User.php';

class UserRepository extends Repository
{
    public function getUser(string $email = null): ?User
    {
        if(is_null($email)){
            return null;
        }

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.users WHERE email = :email
        ');
        $stmt->bindParam(':email',$email,PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false){
            return null;
        }

        return new User($user['email'],
                        $user['password'],
                        $user['username']);
    }

    public function getUserbyUsername(string $username = null): ?User
    {
        if(is_null($username)){
            return null;
        }

        $stmt = $this->database->connect()->prepare('
            SELECT * FROM public.users WHERE username = :username
        ');
        $stmt->bindParam(':username',$username,PDO::PARAM_STR);
        $stmt->execute();

        $user = $stmt->fetch(PDO::FETCH_ASSOC);

        if($user == false){
            return null;
        }

        return new User($user['email'],
            $user['password'],
            $user['username']);
    }

    public function addUser(User $user): void
    {
        /*
        $pdo = $this->database->connect();
        $stmtUD = $pdo->prepare('
            INSERT INTO users_details(username)
            VALUES (?, ?);
        ');

        $pdo->beginTransaction();

        try
        {
            $stmtUD->execute([
                $user->getUsername()
            ]);

            $pdo->commit();
        }
        catch (Exception $e)
        {
            $pdo->rollBack();
        }

        $id = $this->getUserDetailsId($user->getName(), $user->getSurname());
        */
        $pdo = $this->database->connect();
        $stmtU = $pdo->prepare('
            INSERT INTO users(email, password, username)
            VALUES (?, ?, ?);
        ');

        $pdo->beginTransaction();

        try
        {
            $stmtU->execute([
                $user->getEmail(),
                $user->getPassword(),
                $user->getUsername()
            ]);

            $pdo->commit();
        }
        catch (Exception $e)
        {
            $pdo->rollBack();
        }

    }
}