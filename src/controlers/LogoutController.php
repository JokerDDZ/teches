<?php

require_once 'AppControler.php';

class LogoutController extends AppControler{
    public function logout()
    {
        session_start();
        session_destroy();
        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}");
    }
}