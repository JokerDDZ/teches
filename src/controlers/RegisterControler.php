<?php

require_once 'AppControler.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../models/User.php';

class RegisterControler extends AppControler
{
    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function register()
    {
        $userRepository = new UserRepository();

        if(!$this->isPost()){
            return $this->render('register');
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        $username = $_POST['username'];
        $confrimPassword = $_POST['confirmedPassword'];

        $user = $userRepository->getUser($email);

        if(!$user){
            $user = $userRepository->getUserbyUsername($username);
        }

        if($user){
            return $this->render('register',['messages'=>['User already exist']]);
        }

        if($password !== $confrimPassword)
        {
            return $this->render('register',['messages'=>['Wrong confirmed password']]);
        }

        $this->addUser($email,$password,$username);
    }

    public function addUser($email,$password,$username)
    {
        if($this->isPost()) {
            $user = new User($email, md5($password), $username);
            $this->userRepository->addUser($user);
            return $this->render('projects');
        }else {
            //$this->render('register', ['messages'=> $this->messages]);
        }
    }
}