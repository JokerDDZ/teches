<?php

require_once 'AppControler.php';
require_once __DIR__.'/../repository/UserRepository.php';
require_once __DIR__.'/../models/User.php';

class SecurityControler extends AppControler
{
    public function login()
    {
        $userRepository = new UserRepository();

         if(!$this->isPost()){
             return $this->render('login');
         }

        $username = $_POST['username'];
        $password = $_POST['password'];
        $user = $userRepository->getUserbyUsername($username);

        if(!$user){
            return $this->render('login',['messages'=>['User not exist']]);
        }

        if($user->getUsername() !== $username)
        {
            return $this->render('login',['messages'=>['User with this email not exist']]);
        }

        if($user->getPassword() !== md5($password))
        {
            return $this->render('login',['messages'=>['wrong password']]);
        }

        session_write_close();
        session_start();
        $_SESSION['user'] = $user;


        return $this->render('projects');
    }
}