<?php

require_once 'AppControler.php';

class DefaultControler extends AppControler{
    public function index(){
        $this->render('login');
    }

    public function projects(){
        session_start();
        $User = $_SESSION["user"];
        //echo($User->getUsername());
        $this->render('projects');
    }

    public function search(){

    }

    public  function LoginMenu()
    {
        $this->render('login');
    }

    public function logout()
    {
        session_destroy();

        $url = "http://$_SERVER[HTTP_HOST]";
        header("Location: {$url}");
    }
}